# Python Google Speech Video Transcription Tool
Modded transcription service which takes any inputs in the inputs folder and outputs them fully converted to wavs and transcribed via google api. 

You must have google default application credentials provisioned, the directories "inputs, tmp, outputs" all created. You must install the required python modules. You must have ffmpeg installed

To install the required python modules, it is recommended you use a virtualenv. There is a requirements.txt file included for your convience

```
usage: google-speech.py [-h] [--path PATH]

optional arguments:
  -h, --help   show this help message and exit
  --path PATH  Folder for files to be converted and recognized or GCS path for
               files to be recognized
Example usage:
    python google-speech.py ./inputs
    python google-speech.py gs://cloud-samples-tests/speech/vr.wav
```
