#!/usr/bin/env python


"""Modded transcription service which takes any inputs in the inputs folder and outputs them fully converted to wavs and transcribed via google api. 
You must have google default application credentials provisioned, the directories "inputs, tmp, outputs" all created. You must install the required python modules. You must have ffmpeg installed
Example usage:
    python google-speech.py ./inputs
    python google-speech.py gs://cloud-samples-tests/speech/vr.wav
"""

import argparse
import io
import os
import subprocess
import shutil

DEFAULT_INPUT_PATH='./inputs'
DEFAULT_OUTPUT_PATH='./outputs'

# [START speech_transcribe_async]
def transcribe_file(speech_file, filename):
    """Transcribe the given audio file asynchronously."""
    from google.cloud import speech

    client = speech.SpeechClient()

    # [START speech_python_migration_async_request]
    with io.open(speech_file, "rb") as audio_file:
        content = audio_file.read()

    """
     Note that transcription is limited to a 60 seconds audio file.
     Use a GCS file for audio longer than 1 minute.
    """
    audio = speech.RecognitionAudio(content=content)

    config = speech.RecognitionConfig(
        encoding=speech.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=44100,
        language_code="en-US",
    )

    # [START speech_python_migration_async_response]

    operation = client.long_running_recognize(config=config, audio=audio)
    # [END speech_python_migration_async_request]

    print("Waiting for operation to complete...")
    response = operation.result(timeout=90)

    # Each result is for a consecutive portion of the audio. Iterate through
    # them to get the transcripts for the entire audio file.
    transcription_file = open(DEFAULT_OUTPUT_PATH+'/' + filename+ '.txt', 'a')
    for result in response.results:
        # The first alternative is the most likely one for this portion.
        print(u"Transcript: {}".format(result.alternatives[0].transcript))
        transcription_file.write("%s\n" % format(result.alternatives[0].transcript))
    transcription_file.close()

        #print("Confidence: {}".format(result.alternatives[0].confidence))
    # [END speech_python_migration_async_response]

# [END speech_transcribe_async]


# [START speech_transcribe_async_gcs]
def transcribe_gcs(gcs_uri, filename):
    """Asynchronously transcribes the audio file specified by the gcs_uri."""
    from google.cloud import speech

    client = speech.SpeechClient()

    audio = speech.RecognitionAudio(uri=gcs_uri)
    config = speech.RecognitionConfig(
        #encoding=speech.RecognitionConfig.AudioEncoding.WAV,
        sample_rate_hertz=48000,
        language_code="en-US",
    )

    operation = client.long_running_recognize(config=config, audio=audio)

    print("Waiting for operation to complete...")
    response = operation.result(timeout=300)

    # Each result is for a consecutive portion of the audio. Iterate through
    # them to get the transcripts for the entire audio file.
    transcription_file = open(DEFAULT_OUTPUT_PATH+'/' + filename+ '.txt', 'a')
    for result in response.results:
        # The first alternative is the most likely one for this portion.
        print(u"Transcript: {}".format(result.alternatives[0].transcript))
        transcription_file.write("%s\n" % format(result.alternatives[0].transcript))
    transcription_file.close()


# [END speech_transcribe_async_gcs]


def encode_with_ffmpeg(filename):
    encode_with_ffmpeg = "ffmpeg -i " + DEFAULT_INPUT_PATH + "/" + filename + " -vn -acodec pcm_s16le -ar 44100 " + DEFAULT_OUTPUT_PATH + "/" + filename + ".wav"
    subprocess.call(encode_with_ffmpeg, shell=True)
    print("Converted file :" + filename + " to " + filename + ".wav")

def split_to_chunks(filename):
    split_into_chunks = "ffmpeg -i " + DEFAULT_OUTPUT_PATH + "/" + filename + ".wav" + " -f segment -segment_time 50 -c copy ./tmp/temp%03d.wav"
    subprocess.call(split_into_chunks, shell=True)

def convert_folder(folder):
    for filename in sorted(os.listdir(folder)):
        if not filename.startswith('.'):
            encode_with_ffmpeg(filename)
            split_to_chunks(filename)
            for chunk in os.listdir("./tmp"):
                transcribe_file("./tmp/" +chunk, filename)
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", help="Folder for files to be converted and recognized or GCS path for files to be recognized")
    args = parser.parse_args()
    if args.path is not None:
        if args.path.startswith("gs://"):
            transcribe_gcs(args.path)
        else:
            #we are modding this to check a directory and run this transcription for each file in the directory.
            #transcribe_file(args.path)
            convert_folder(args.path)
    else:
        convert_folder(DEFAULT_INPUT_PATH)

